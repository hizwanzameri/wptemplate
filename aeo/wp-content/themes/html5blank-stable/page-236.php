<?php /* Template Name: Brand Template */ ?>
<html lang="ja">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=960">
  <meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="イオンファンタジー,Mollyfantasy,モーリーファンタジー,室内遊園地,インプレ,インドア・プレーグラウンド,ララちゃん,イオくん,カードゲーム,UFOキャッチャー,イオン,イオングループ" />
<meta name="description" content="ファミリー向け室内遊園地を全国のショッピングセンター内で展開。Mollyfantasy（モーリーファンタジー）遊技機器の紹介等。" />
  <title>House Brand</title>
  <!-- OGP -->
	<meta property="og:locale" content="UTF-8" />
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />
	<meta property="twitter:image" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<!-- ICON -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/image/favicon.ico">

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">

  <!-- JS -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/aeonfantasy.js"></script>
	<!--[if lt IE 9]>
	<script src="../js/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript">
	$(function(){

		//数値を編集してください。合計は自動的に計算されます。
		var stores_Japan = 502;
		var stores_China = 89;
		var stores_Vietnam = 4;
		var stores_Malaysia = 58;
		var stores_Cambodia = 2;
		var stores_Thailand = 38;
		var stores_Philippines = 5;
		var stores_Indonesia = 3;

		//ここから先は編集しないでください
		var total = stores_Japan + stores_China + stores_Vietnam + stores_Malaysia + stores_Cambodia + stores_Thailand + stores_Philippines + stores_Indonesia;
		$("#map-asia #japan dd").append(stores_Japan);
		$("#map-asia #china dd").append(stores_China);
		$("#map-asia #vietnam dd").append(stores_Vietnam);
		$("#map-asia #malaysia dd").append(stores_Malaysia);
		$("#map-asia #cambodia dd").append(stores_Cambodia);
		$("#map-asia #thai dd").append(stores_Thailand);
		$("#map-asia #philippines dd").append(stores_Philippines);
		$("#map-asia #indonesia dd").append(stores_Indonesia);
		$("#map-asia #total dd").append(total);

	});
	</script>
	<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/cloud.fulkrum.net\/labs\/aeo\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.2"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='jquery.prettyphoto-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/css/prettyPhoto.css?ver=4.2.2' media='all' />
<link rel='stylesheet' id='video-lightbox-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/wp-video-lightbox.css?ver=4.2.2' media='all' />
<link rel='stylesheet' id='rtbs-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/responsive-tabs/css/rtbs_style.min.css?ver=4.2.2' media='all' />
<link rel='stylesheet' id='tabby.css-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/tabby-responsive-tabs/css/tabby.css?ver=1.2.1' media='all' />
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/themes/html5blank-stable/js/scripts.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/js/jquery.prettyPhoto.js?ver=3.1.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var vlpp_vars = {"prettyPhoto_rel":"wp-video-lightbox","animation_speed":"fast","slideshow":"5000","autoplay_slideshow":"false","opacity":"0.80","show_title":"true","allow_resize":"true","allow_expand":"true","default_width":"640","default_height":"480","counter_separator_label":"\/","theme":"pp_default","horizontal_padding":"20","hideflash":"false","wmode":"opaque","autoplay":"false","modal":"false","deeplinking":"false","overlay_gallery":"true","overlay_gallery_max":"30","keyboard_shortcuts":"true","ie6_fallback":"true"};
/* ]]> */
</script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/js/video-lightbox.js?ver=3.1.6'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/responsive-tabs/js/rtbs.min.js?ver=4.2.2'></script>
</head>
<body id="shop" class="page">
	<div id="container">
		<header id="header">
			<h1 id="siteid"><a href="/labs/aeo"><img src="<?php echo get_template_directory_uri(); ?>/images/siteid.png" width="244" height="160" alt="Aeon Fantasy"></a></h1>
		<!-- gloval navigation -->
<nav id="primary_nav_wrap">
<ul>
  <li class="current-menu-item"><a href="/labs/aeo">Home</a></li>
  <li class="current-menu-item"><a href="#">About Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(13); ?>">Corporate Info</a></li>
      <li><a href="<?php echo get_page_link(36); ?>">Top Message</a></li>
      <li><a href="<?php echo get_page_link(80); ?>">Work With Us</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="#">What's New</a>
    <ul>
      <li><a href="<?php echo get_page_link(85); ?>">Events</a></li>
      <li><a href="<?php echo get_page_link(8); ?>">New Promotion</a></li>
      <li><a href="<?php echo get_page_link(114); ?>">Birthday Packages</a></li>
      <li><a href="<?php echo get_page_link(331); ?>">Group Visit Packages</a></li>
      <li><a href="<?php echo get_page_link(339); ?>">New Store Opening</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="<?php echo get_page_link(236); ?>">House Brand</a></li>
  <li class="current-menu-item"><a href="#">Playground</a>
    <ul>
      <li><a href="<?php echo get_page_link(42); ?>">Character</a></li>
      <li><a href="<?php echo get_page_link(189); ?>">Games</a></li>
      <li><a href="<?php echo get_page_link(193); ?>">Song</a></li>
      <li><a href="<?php echo get_page_link(195); ?>">Video</a></li>
      <li><a href="<?php echo get_page_link(249); ?>">Wallpaper</a></li>
    </ul>
    </li>
  <li class="current-menu-item"><a href="#">Contact Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(71); ?>">Headquarter</a></li>
      <li><a href="<?php echo get_page_link(164); ?>">Stores</a></li>
    </ul>
    </li>
</ul>
</nav>
		</header>
		<div id="headtree"></div>
		<section id="shop-index-block">
			<!--<h2 class="ttl-page"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/ttl-shop.png" width="323" height="77" alt="店舗一覧"></h2>-->
			<div id="shop-house">

				<!-- +++++++ MAP ASIA +++++++ -->
				<div id="map-asia">
				<!--
					<dl id="date">
						<dt>2015年1月31日現在</dt>
					</dl>-->
					<dl id="japan">
						<dt><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-japan_off.png" width="83" height="29" alt="Japan"></dt>
						<dd></dd>
					</dl>
					<dl id="china">
						<dt><a href="http://www.aeonfantasy.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-china_off.png" width="89" height="28" alt="China"></a></dt>
						<dd></dd>
					</dl>
					<dl id="vietnam">
						<dt><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-vietnam_off.png" width="105" height="28" alt="Vietnam"></dt>
						<dd></dd>
					</dl>
					<dl id="malaysia">
						<dt><a href="http://www.aeonfantasy.com.my/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-malaysia_off.png" width="113" height="28" alt="Malaysia"></a></dt>
						<dd></dd>
					</dl>
					<dl id="cambodia">
						<dt><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-cambodia_off.png" width="123" height="28" alt="Cambodia"></dt>
						<dd></dd>
					</dl>
					<dl id="thai">
						<dt><a href="http://www.aeonfantasy.co.th/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-thai_off.png" width="113" height="28" alt="Thailand"></a></dt>
						<dd></dd>
					</dl>
					<dl id="philippines">
						<dt><a href="http://www.aeonfantasy.com.ph/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-philippines_off.png" width="119" height="29" alt="Philippines"></a></dt>
						<dd></dd>
					</dl>
					<dl id="indonesia">
						<dt><a href="http://www.aeonfantasy.co.id/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-indonesia_off.png" width="119" height="28" alt="Indonesia"></a></dt>
						<dd></dd>
					</dl>
					<dl id="total">
						<dt><img src="<?php echo get_template_directory_uri(); ?>/brand/images/name-total.png" width="96" height="32" alt="Total"></dt>
						<dd></dd>
					</dl>
					<!--<p class="btn-map"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/change-japan_off.png" width="266" height="183" alt="日本地図を拡大"></p>-->
				</div><!-- //#map-world -->

				<!-- +++++++ MAP JAPAN +++++++ -->
				<div id="map-japan" class="hide">
					<ul>
						<li id="btn-hokkaido"><a href="hokkaido.html"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/btn-hokkaido_off.png" width="172" height="37" alt="北海道・東北エリア"></a></li>
						<li id="btn-chubu"><a href="chubu.html"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/btn-chubu_off.png" width="172" height="37" alt="中部エリア"></a></li>
						<li id="btn-kanto"><a href="kanto.html"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/btn-kanto_off.png" width="172" height="37" alt="関東エリア"></a></li>
						<li id="btn-kinki"><a href="kinki.html"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/btn-kinki_off.png" width="172" height="37" alt="近畿エリア"></a></li>
						<li id="btn-shikoku"><a href="shikoku.html"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/btn-shikoku_off.png" width="172" height="37" alt="四国・中国エリア"></a></li>
						<li id="btn-kyushu"><a href="kyushu.html"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/btn-kyushu_off.png" width="172" height="37" alt="九州・沖縄エリア"></a></li>
					</ul>
					<!--<p class="btn-map"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/change-asia_off.png" width="333" height="194" alt="世界地図にもどる"></p>-->
				</div><!-- //#map-japan -->


			</div>
		</section><!-- //#shop-index-block -->

		<div id="content-area">
			<section id="brand-block1">
				<!--<h2 class="ttl-page"><img src="<?php echo get_template_directory_uri(); ?>/brand/images/ttl-brand.png" width="323" height="77" alt="施設紹介"></h2>-->

<main role="main">
		<!-- section -->
		<section>

			<h1 class="h1page"><?php the_title(); ?></h1>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>
				
			</section><!-- //#brand-block -->
		</div>
	</div>



	


<?php get_footer(); ?>
