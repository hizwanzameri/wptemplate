<?php get_header(); ?>

<body id="event" class="page">
	<div id="container">
		<header id="header">
			<h1 id="siteid"><a href="/labs/aeo"><img src="<?php echo get_template_directory_uri(); ?>/images/siteidS.png" width="174" height="108" alt="Aeon Fantasy"></a></h1>
		<!-- gloval navigation 
			<nav id="gnav">
				<ul>
					<li class="hnav-home"><a href="<?php get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-home_off.png" width="88" height="50" alt="ホーム"></a></li><!--
					--<li class="hnav-event"><a href="<?php echo get_page_link(8); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-new_off.png" width="100" height="50" alt=""></a></li><!--
					--<li><a href="<?php echo get_page_link(78); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-play_off.png" width="100" height="50" alt=""></a></li><!--
					--<li><a href="<?php echo get_page_link(71); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-location_off.png" width="88" height="50" alt=""></a></li><!--
					--<li><a href="<?php echo get_page_link(76); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-contact_off.png" alt="English" width="88" height="50"></a></li><!--
					--<li><a href="<?php echo get_page_link(13); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-about_off.png" width="88" height="50" alt=""></a></li>
				</ul>
			</nav>-->
			<nav id="primary_nav_wrap">
<ul>
  <li class="current-menu-item"><a href="/labs/aeo">Home</a></li>
  <li class="current-menu-item"><a href="#">About Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(13); ?>">Corporate Info</a></li>
      <li><a href="<?php echo get_page_link(36); ?>">Top Message</a></li>
      <li><a href="<?php echo get_page_link(80); ?>">Work With Us</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="#">What's New</a>
    <ul>
      <li><a href="<?php echo get_page_link(85); ?>">Events</a></li>
      <li><a href="<?php echo get_page_link(8); ?>">New Promotion</a></li>
      <li><a href="<?php echo get_page_link(114); ?>">Birthday Packages</a></li>
      <li><a href="<?php echo get_page_link(331); ?>">Group Visit Packages</a></li>
      <li><a href="<?php echo get_page_link(339); ?>">New Store Opening</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="<?php echo get_page_link(236); ?>">House Brand</a></li>
  <li class="current-menu-item"><a href="#">Playground</a>
    <ul>
      <li><a href="<?php echo get_page_link(42); ?>">Character</a></li>
      <li><a href="<?php echo get_page_link(189); ?>">Games</a></li>
      <li><a href="<?php echo get_page_link(193); ?>">Song</a></li>
      <li><a href="<?php echo get_page_link(195); ?>">Video</a></li>
      <li><a href="<?php echo get_page_link(249); ?>">Wallpaper</a></li>
    </ul>
    </li>
  <li class="current-menu-item"><a href="#">Contact Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(71); ?>">Headquarter</a></li>
      <li><a href="<?php echo get_page_link(164); ?>">Stores</a></li>
    </ul>
    </li>
</ul>
</nav>
		</header>
		<div id="headtree"></div>
		<div id="content-area" class="content-page">


<main role="main">
		<!-- section -->
		<section>

			<h1 class="h1page"><?php the_title(); ?></h1>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>



		</div>
	</div>


	


<?php get_footer(); ?>
