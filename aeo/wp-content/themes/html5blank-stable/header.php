<!doctype html>
<html lang="ja">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=960">
  <meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="イオンファンタジー,Mollyfantasy,モーリーファンタジー,室内遊園地,インプレ,インドア・プレーグラウンド,ララちゃん,イオくん,カードゲーム,UFOキャッチャー,イオン,イオングループ" />
<meta name="description" content="ファミリー向け室内遊園地を全国のショッピングセンター内で展開。Mollyfantasy（モーリーファンタジー）遊技機器の紹介等。" />
  <title>Mollyfantasy Malaysia</title>
  <!-- OGP -->
	<meta property="og:locale" content="UTF-8" />
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />
	<meta property="twitter:image" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<!-- ICON -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/image/favicon.ico">

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
  <!-- JS -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/aeonfantasy.js"></script>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<![endif]-->

	<!--
	<script type="text/javascript">
// <![CDATA[
    if (navigator.userAgent.indexOf('iPhone') != -1) { location.href="<?php echo get_template_directory_uri(); ?>/sp/index.html"; }
    else if (navigator.userAgent.indexOf('iPod')!=-1){ location.href="<?php echo get_template_directory_uri(); ?>/sp/index.html"; }
    else if  (navigator.userAgent.indexOf('Android') != -1) { location.href="<?php echo get_template_directory_uri(); ?>/sp/index.html"; }
    else if (navigator.userAgent.indexOf('Windows CE')!=-1){ location.href="<?php echo get_template_directory_uri(); ?>/sp/index.html"; }
// ]]>
	-->
</script>
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/cloud.fulkrum.net\/labs\/aeo\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.2"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='jquery.prettyphoto-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/css/prettyPhoto.css?ver=4.2.2' media='all' />
<link rel='stylesheet' id='video-lightbox-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/wp-video-lightbox.css?ver=4.2.2' media='all' />
<link rel='stylesheet' id='rtbs-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/responsive-tabs/css/rtbs_style.min.css?ver=4.2.2' media='all' />
<link rel='stylesheet' id='tabby.css-css'  href='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/tabby-responsive-tabs/css/tabby.css?ver=1.2.1' media='all' />
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/themes/html5blank-stable/js/scripts.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/js/jquery.prettyPhoto.js?ver=3.1.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var vlpp_vars = {"prettyPhoto_rel":"wp-video-lightbox","animation_speed":"fast","slideshow":"5000","autoplay_slideshow":"false","opacity":"0.80","show_title":"true","allow_resize":"true","allow_expand":"true","default_width":"640","default_height":"480","counter_separator_label":"\/","theme":"pp_default","horizontal_padding":"20","hideflash":"false","wmode":"opaque","autoplay":"false","modal":"false","deeplinking":"false","overlay_gallery":"true","overlay_gallery_max":"30","keyboard_shortcuts":"true","ie6_fallback":"true"};
/* ]]> */
</script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/wp-video-lightbox/js/video-lightbox.js?ver=3.1.6'></script>
<script type='text/javascript' src='http://cloud.fulkrum.net/labs/aeo/wp-content/plugins/responsive-tabs/js/rtbs.min.js?ver=4.2.2'></script>
</head>
