// image on_off
$(function(){
    $('a img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
          if (!$(this).hasClass('current')) {
          $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
    });
    $('.btn-map img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
          if (!$(this).hasClass('current')) {
          $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
    });
    $('#japan dt img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
          if (!$(this).hasClass('current')) {
          $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
    });
    $('#china dt img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
          if (!$(this).hasClass('current')) {
          $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
    });
    $('#malaysia dt img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
          if (!$(this).hasClass('current')) {
          $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
    });
    $('#thai dt img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
          if (!$(this).hasClass('current')) {
          $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
    });
    $('#philippines dt img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
          if (!$(this).hasClass('current')) {
          $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
    });

    $("#map-asia p.btn-map").click(function() {
        $("#map-asia").addClass('hide');
        $("#map-japan").removeClass('hide');
    });
    $("#map-japan p.btn-map").click(function() {
        $("#map-japan").addClass('hide');
        $("#map-asia").removeClass('hide');
    });
    $("#japan dt").click(function() {
        $("#map-asia").addClass('hide');
        $("#map-japan").removeClass('hide');
    });

    if($("body#home").get(0)){
        AEONFANTASYHOME.init();
    }else if($("body#movie").get(0)){
        AEONFANTASYMOVIE.init();
    }else if($("body#workshop").get(0)){
        AEONFANTASYWORKSHOP.init();
    }else if($("body#library").get(0)){
        AEONFANTASYLIBRARY.init();
    }else if($("body#closet").get(0)){
        AEONFANTASYCLOSET.init();
    }
});

AEONFANTASYHOME = {
    animates : [
        {
            sel:"#anime-AEO",
            btn:"aeo",
            width:176,
            height:152,
            hover:false,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_IO_idling.png",
                frames:120,
                start:1,
                loop:120,
                current:1
            },
            over:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_IO_mouseover.png",
                frames:150,
                start:13,
                loop:128,
                current:1
            },
        },
        {
            sel:"#anime-LALA",
            btn:"lala",
            width:170,
            height:150,
            hover:false,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_LALA_idling.png",
                frames:120,
                start:1,
                loop:120,
                current:1
            },
            over:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_LALA_mouseover.png",
                frames:178,
                start:21,
                loop:153,
                current:1
            },
        },
        {
            sel:"#anime-PYUTAN",
            btn:"pyutan",
            width:76,
            height:88,
            hover:false,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_pyu_idling.png",
                frames:96,
                start:1,
                loop:96,
                current:1
            },
            over:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_pyu_mouseover.png",
                frames:94,
                start:15,
                loop:79,
                current:1
            },
        },
        {
            sel:"#anime-PANBIT",
            btn:"panbit",
            width:160,
            height:128,
            hover:false,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_pan_idling.png",
                frames:96,
                start:1,
                loop:96,
                current:1
            },
            over:{
                path:"wp-content/themes/html5blank-stable/images/movies/main/Main_pan_mouseover.png",
                frames:144,
                start:1,
                loop:144,
                current:1
            },
        }
    ],

    init: function(){
        AEONFANTASYCOMMON.preload([
            AEONFANTASYHOME.animates[0].idle.path,
            AEONFANTASYHOME.animates[0].over.path,
            AEONFANTASYHOME.animates[1].idle.path,
            AEONFANTASYHOME.animates[1].over.path,
            AEONFANTASYHOME.animates[2].idle.path,
            AEONFANTASYHOME.animates[2].over.path,
            AEONFANTASYHOME.animates[3].idle.path,
            AEONFANTASYHOME.animates[3].over.path,
        ], function(total, loaded){
            if (loaded >= total) {
                $('.loading').fadeOut('slow', function() {
                    $('.loading').css("display","none");
                });
                for (var i = 0; i < AEONFANTASYHOME.animates.length; i++) {
                    var animateobj =  AEONFANTASYHOME.animates[i];
                    $('.button-action.' + animateobj.btn).hover(
                        function(){
                            $(this).addClass('over');
                            var idx = $('.button-action').index(this);
                            var overobj = AEONFANTASYHOME.animates[idx];
                            overobj.hover = true;
                            overobj.over.current = 1;
                        },
                        function(){
                             $(this).removeClass('over');
                            var idx = $('.button-action').index(this);
                            var overobj = AEONFANTASYHOME.animates[idx];
                            overobj.hover  = false;
                        }
                    );
                    $(animateobj.sel + " .mov .idle").css("background-image","url(" + animateobj.idle.path + ")");
                    $(animateobj.sel + " .mov .over").css("background-image","url(" + animateobj.over.path + ")");
                }

                setInterval(function(){
                for (var i = 0; i < AEONFANTASYHOME.animates.length; i++) {
                        var animateobj =  AEONFANTASYHOME.animates[i];
                        if(!animateobj.hover){
                            animateobj.idle.current ++;
                            if(animateobj.idle.current > animateobj.idle.loop){
                                animateobj.idle.current = animateobj.idle.start;
                            }
                        }else{
                            animateobj.over.current ++;
                            if(animateobj.over.current > animateobj.over.loop){
                                animateobj.over.current = animateobj.over.start;
                            }
                        }

                        var pxi = (animateobj.idle.frames * animateobj.width) -(animateobj.idle.current - 1) * animateobj.width;
                        var pxo = (animateobj.over.frames * animateobj.width) - (animateobj.over.current - 1) * animateobj.width;

                        $(animateobj.sel + " .mov .idle").css("background-position", pxi + "px " + 0 +  "px");
                        $(animateobj.sel + " .mov .over").css("background-position", pxo + "px " + 0 +  "px");
                    }
                }, 1000 / 24);
            }
        });
    }
};

AEONFANTASYMOVIE = {
    animates : [
        {
            sel:"#movie-content-area",
            width:960,
            height:510,
            hover:false,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/theater/THEATER_inmotion_t01_02.png",
                frames:130,
                current:1
            }
        },
    ],

    init: function(){
        AEONFANTASYCOMMON.preload([
            AEONFANTASYMOVIE.animates[0].idle.path,
        ], function(total, loaded){
            if (loaded >= total) {
                $('.loading').fadeOut('slow', function() {
                    $('.loading').css("display","none");
                });
                var animateobj =  AEONFANTASYMOVIE.animates[0];
                $(animateobj.sel + " .mov").css("background-image","url(" + animateobj.idle.path + ")");

                setInterval(function(){
                    var animateobj =  AEONFANTASYMOVIE.animates[0];
                    animateobj.idle.current ++;
                    if(animateobj.idle.current > animateobj.idle.frames){
                        animateobj.idle.current = animateobj.idle.frames;
                    }
                    var pxi = (animateobj.idle.frames * animateobj.width) - (animateobj.idle.current - 1) * animateobj.width ;
                    $(animateobj.sel + " .mov").css("background-position", pxi + "px " + 0 +  "px");
                }, 1000 / 24);
            }
        });
    }
};

AEONFANTASYWORKSHOP = {
    animates : [
        {
            sel:"#workshop-content-area",
            width:400,
            height:200,
            hover:0,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/workshop/Workshop_PYU_idling_all.png",
                frames:132,
                start:57,
                loop:120,
                current:1
            },
            overboad:{
                path:"wp-content/themes/html5blank-stable/images/movies/workshop/Workshop_PYU_idling_point_board.png",
                frames:46,
                start:25,
                loop:35,
                current:1
            },
            overtable:{
                path:"wp-content/themes/html5blank-stable/images/movies/workshop/Workshop_PYU_idling_point_table.png",
                frames:46,
                start:1,
                loop:46,
                current:1
            }
        },
    ],

    init: function(){
        AEONFANTASYCOMMON.preload([
            AEONFANTASYWORKSHOP.animates[0].idle.path,
            AEONFANTASYWORKSHOP.animates[0].overboad.path,
            AEONFANTASYWORKSHOP.animates[0].overtable.path,
        ], function(total, loaded){
            if (loaded >= total) {
                $('.loading').fadeOut('slow', function() {
                    $('.loading').css("display","none");
                });

                var animateobj =  AEONFANTASYWORKSHOP.animates[0];
                $(animateobj.sel + " .mov .idle").css("background-image","url(" + animateobj.idle.path + ")");
                $(animateobj.sel + " .mov .overboad").css("background-image","url(" + animateobj.overboad.path + ")");
                $(animateobj.sel + " .mov .overtable").css("background-image","url(" + animateobj.overtable.path + ")");

                $("#workshop-box01").hover(
                    function(){
                        $("#workshop-content-area .mov").addClass('overboad');
                        AEONFANTASYWORKSHOP.animates[0].hover = 1;
                        AEONFANTASYWORKSHOP.animates[0].overboad.current = 1;
                    },
                    function(){
                        $("#workshop-content-area .mov").removeClass('overboad');
                        var idx = $('.button-action').index(this);
                        AEONFANTASYWORKSHOP.animates[0].hover  = 0;
                    }
                );

                $("#workshop-box02").hover(
                    function(){
                        $("#workshop-content-area .mov").addClass('overtable');
                        AEONFANTASYWORKSHOP.animates[0].hover = 2;
                        AEONFANTASYWORKSHOP.animates[0].overtable.current = 1;
                    },
                    function(){
                        $("#workshop-content-area .mov").removeClass('overtable');
                        var idx = $('.button-action').index(this);
                        AEONFANTASYWORKSHOP.animates[0].hover  = 0;
                    }
                );

                setInterval(function(){
                    var animateobj =  AEONFANTASYWORKSHOP.animates[0];
                    if(animateobj.hover == 0){
                        animateobj.idle.current ++;
                        if(animateobj.idle.current > animateobj.idle.loop){
                            animateobj.idle.current = animateobj.idle.start;
                        }
                    }else if(animateobj.hover == 1){
                        animateobj.overboad.current ++;
                        if(animateobj.overboad.current > animateobj.overboad.loop){
                            animateobj.overboad.current = animateobj.overboad.start;
                        }
                    }else if(animateobj.hover == 2){
                        animateobj.overtable.current ++;
                        if(animateobj.overtable.current > animateobj.overtable.loop){
                            animateobj.overtable.current = animateobj.overtable.loop;
                        }
                    }
                    var pxi = (animateobj.idle.frames * animateobj.width) - (animateobj.idle.current - 1) * animateobj.width;
                    var pxb = (animateobj.overboad.frames * animateobj.width) - (animateobj.overboad.current - 1) * animateobj.width;
                    var pxt = (animateobj.overtable.frames * animateobj.width) - (animateobj.overtable.current - 1) * animateobj.width;
                    $(animateobj.sel + " .mov .idle").css("background-position", pxi + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overtable").css("background-position", pxt + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overboad").css("background-position", pxb + "px " + 0 +  "px");

                }, 1000 / 24);
            }
        });
    }
};


AEONFANTASYLIBRARY = {
    animates : [
        {
            sel:"#library-content-area",
            width:200,
            height:250,
            hover:0,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/library/IOroom_onmouse_loop.png",
                frames:120,
                start:1,
                loop:120,
                current:1
            },
            overbook:{
                path:"wp-content/themes/html5blank-stable/images/movies/library/IOroom_onmouse_picturebook.png",
                frames:150,
                start:45,
                loop:73,
                current:1
            },
            overmusic:{
                path:"wp-content/themes/html5blank-stable/images/movies/library/IOroom_onmouse_music.png",
                frames:150,
                start:43,
                loop:74,
                current:1
            }
        },
    ],

    init: function(){
        AEONFANTASYCOMMON.preload([
            AEONFANTASYLIBRARY.animates[0].idle.path,
            AEONFANTASYLIBRARY.animates[0].overmusic.path,
            AEONFANTASYLIBRARY.animates[0].overbook.path,
        ], function(total, loaded){
            if (loaded >= total) {
                $('.loading').fadeOut('slow', function() {
                    $('.loading').css("display","none");
                });
                var animateobj =  AEONFANTASYLIBRARY.animates[0];
                $(animateobj.sel + " .mov .idle").css("background-image","url(" + animateobj.idle.path + ")");
                $(animateobj.sel + " .mov .overmusic").css("background-image","url(" + animateobj.overmusic.path + ")");
                $(animateobj.sel + " .mov .overbook").css("background-image","url(" + animateobj.overbook.path + ")");

                $("#library-box02").hover(
                    function(){
                        $("#library-content-area .mov").addClass('overmusic');
                        AEONFANTASYLIBRARY.animates[0].hover = 1;
                        AEONFANTASYLIBRARY.animates[0].overmusic.current = 1;
                    },
                    function(){
                        $("#library-content-area .mov").removeClass('overmusic');
                        var idx = $('.button-action').index(this);
                        AEONFANTASYLIBRARY.animates[0].hover  = 0;
                    }
                );

                $("#library-box01").hover(
                    function(){
                        $("#library-content-area .mov").addClass('overbook');
                        AEONFANTASYLIBRARY.animates[0].hover = 2;
                        AEONFANTASYLIBRARY.animates[0].overbook.current = 2;
                    },
                    function(){
                        $("#library-content-area .mov").removeClass('overbook');
                        var idx = $('.button-action').index(this);
                        AEONFANTASYLIBRARY.animates[0].hover  = 0;
                    }
                );

                setInterval(function(){
                    var animateobj =  AEONFANTASYLIBRARY.animates[0];
                    if(animateobj.hover == 0){
                        animateobj.idle.current ++;
                        if(animateobj.idle.current > animateobj.idle.loop){
                            animateobj.idle.current = animateobj.idle.start;
                        }
                    }else if(animateobj.hover == 1){
                        animateobj.overmusic.current ++;
                        if(animateobj.overmusic.current > animateobj.overmusic.loop){
                            animateobj.overmusic.current = animateobj.overmusic.start;
                        }
                    }else if(animateobj.hover == 2){
                        animateobj.overbook.current ++;
                        if(animateobj.overbook.current > animateobj.overbook.loop){
                            animateobj.overbook.current = animateobj.overbook.start;
                        }
                    }

                    var pxi = (animateobj.idle.frames * animateobj.width) -(animateobj.idle.current - 1) * animateobj.width;
                    var pxm = (animateobj.overmusic.frames * animateobj.width) - (animateobj.overmusic.current - 1) * animateobj.width;
                    var pxb = (animateobj.overbook.frames * animateobj.width) - (animateobj.overbook.current - 1) * animateobj.width;

                    $(animateobj.sel + " .mov .idle").css("background-position", pxi + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overbook").css("background-position", pxb + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overmusic").css("background-position", pxm + "px " + 0 +  "px");

                }, 1000 / 24);
            }
        });
    }
};



AEONFANTASYCLOSET = {
    animates : [
        {
            sel:"#closet-content-area",
            width:350,
            height:30,
            hover:0,
            idle:{
                path:"wp-content/themes/html5blank-stable/images/movies/closet/LALACloset_idle.png",
                frames:142,
                start:1,
                loop:142,
                current:1
            },
            overa:{
                path:"wp-content/themes/html5blank-stable/images/movies/closet/LALACloset_choose_A.png",
                frames:242,
                start:88,
                loop:242,
                current:1
            },
            overb:{
                path:"wp-content/themes/html5blank-stable/images/movies/closet/LALACloset_choose_B.png",
                frames:242,
                start:88,
                loop:242,
                current:1
            },
            overc:{
                path:"wp-content/themes/html5blank-stable/images/movies/closet/LALACloset_choose_C.png",
                frames:242,
                start:88,
                loop:242,
                current:1
            },
            overd:{
                path:"wp-content/themes/html5blank-stable/images/movies/closet/LALACloset_choose_D.png",
                frames:242,
                start:88,
                loop:242,
                current:1
            },
            overe:{
                path:"wp-content/themes/html5blank-stable/images/movies/closet/LALACloset_choose_E.png",
                frames:242,
                start:88,
                loop:242,
                current:1
            },
        },
    ],
    init: function(){
        AEONFANTASYCOMMON.preload([
            AEONFANTASYCLOSET.animates[0].idle.path,
            AEONFANTASYCLOSET.animates[0].overa.path,
            AEONFANTASYCLOSET.animates[0].overb.path,
            AEONFANTASYCLOSET.animates[0].overc.path,
            AEONFANTASYCLOSET.animates[0].overd.path,
            AEONFANTASYCLOSET.animates[0].overe.path,

            "wp-content/themes/html5blank-stable/images/closet/lala_closet_A.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_A_no.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_B.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_B_no.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_C.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_C_no.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_D.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_D_no.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_E.png",
            "wp-content/themes/html5blank-stable/images/closet/lala_closet_E_no.png",

        ], function(total, loaded){
            if (loaded >= total) {
                $('.loading').fadeOut('slow', function() {
                    $('.loading').css("display","none");
                });
                var animateobj =  AEONFANTASYCLOSET.animates[0];
                $(animateobj.sel + " .mov .idle").css("background-image","url(" + animateobj.idle.path + ")");
                $(animateobj.sel + " .mov .overa").css("background-image","url(" + animateobj.overa.path + ")");
                $(animateobj.sel + " .mov .overb").css("background-image","url(" + animateobj.overb.path + ")");
                $(animateobj.sel + " .mov .overc").css("background-image","url(" + animateobj.overc.path + ")");
                $(animateobj.sel + " .mov .overd").css("background-image","url(" + animateobj.overd.path + ")");
                $(animateobj.sel + " .mov .overe").css("background-image","url(" + animateobj.overe.path + ")");

                $("#closet-box02").mousemove(function(e){
                    var ox = e.offsetX;
                    var o0 = 91;
                    var o1 = 136;
                    var o2 = 160;
                    var o3 = 192;
                    var o4 = 255;
                    console.log(ox);
                    var animateobj =  AEONFANTASYCLOSET.animates[0];

                    if(!($(this).hasClass('activea') ||
                        $(this).hasClass('activeb') ||
                        $(this).hasClass('activec') ||
                        $(this).hasClass('actived') ||
                        $(this).hasClass('activee'))){

                        $(this).removeClass('overa');
                        $(this).removeClass('overb');
                        $(this).removeClass('overc');
                        $(this).removeClass('overd');
                        $(this).removeClass('overe');

                        if(ox >= 0 && ox < o0){
                            $(this).addClass('overa');
                        }else if(ox >= o0 && ox < o1){
                            $(this).addClass('overb');
                        }else if(ox >= o1 && ox < o2){
                            $(this).addClass('overc');
                        }else if(ox >= o2 && ox < o3){
                            $(this).addClass('overd');
                        }else if(ox >= o3 && ox < o4){
                            $(this).addClass('overe');
                        }

                    }

                });

                $("#closet-box02").mouseover(
                    function(){
                        var animateobj =  AEONFANTASYCLOSET.animates[0];
                        if(animateobj.hover != 0){
                            animateobj.hover = 0;

                            $(animateobj.sel + " .mov").removeClass('overa');
                            $(animateobj.sel + " .mov").removeClass('overb');
                            $(animateobj.sel + " .mov").removeClass('overc');
                            $(animateobj.sel + " .mov").removeClass('overd');
                            $(animateobj.sel + " .mov").removeClass('overe');

                            $(this).removeClass('overa');
                            $(this).removeClass('overb');
                            $(this).removeClass('overc');
                            $(this).removeClass('overd');
                            $(this).removeClass('overe');

                            $(this).removeClass('activea');
                            $(this).removeClass('activeb');
                            $(this).removeClass('activec');
                            $(this).removeClass('actived');
                            $(this).removeClass('activee');

                        }
                    }
                );

                $("#closet-box02").click(function(e){
                    var ox = e.offsetX;
                    var o0 = 91;
                    var o1 = 136;
                    var o2 = 160;
                    var o3 = 192;
                    var o4 = 255;

                    var animateobj =  AEONFANTASYCLOSET.animates[0];

                    $(this).removeClass('activea');
                    $(this).removeClass('activeb');
                    $(this).removeClass('activec');
                    $(this).removeClass('actived');
                    $(this).removeClass('activee');

                    $(this).removeClass('overa');
                    $(this).removeClass('overb');
                    $(this).removeClass('overc');
                    $(this).removeClass('overd');
                    $(this).removeClass('overe');

                    if(ox >= 0 && ox < o0){
                        //A
                        if(animateobj.hover != 1){
                            animateobj.hover = 1;
                            animateobj.overa.current = 1;

                            $(animateobj.sel + " .mov").removeClass('overa');
                            $(animateobj.sel + " .mov").removeClass('overb');
                            $(animateobj.sel + " .mov").removeClass('overc');
                            $(animateobj.sel + " .mov").removeClass('overd');
                            $(animateobj.sel + " .mov").removeClass('overe');

                            $(animateobj.sel + " .mov").addClass('overa');
                            $("#closet-box02").addClass('activea');

                        }
                    }else if(ox >= o0 && ox < o1){
                        //B
                        if(animateobj.hover != 2){
                            animateobj.hover = 2;
                            animateobj.overb.current = 1;
                            $(animateobj.sel + " .mov").removeClass('overa');
                            $(animateobj.sel + " .mov").removeClass('overb');
                            $(animateobj.sel + " .mov").removeClass('overc');
                            $(animateobj.sel + " .mov").removeClass('overd');
                            $(animateobj.sel + " .mov").removeClass('overe');
                            $(animateobj.sel + " .mov").addClass('overb');
                            $("#closet-box02").addClass('activeb');
                        }
                    }else if(ox >= o1 && ox < o2){
                        //C
                        if(animateobj.hover != 3){
                            animateobj.hover = 3;
                            animateobj.overc.current = 1;
                            $(animateobj.sel + " .mov").removeClass('overa');
                            $(animateobj.sel + " .mov").removeClass('overb');
                            $(animateobj.sel + " .mov").removeClass('overc');
                            $(animateobj.sel + " .mov").removeClass('overd');
                            $(animateobj.sel + " .mov").removeClass('overe');
                            $(animateobj.sel + " .mov").addClass('overc');
                            $("#closet-box02").addClass('activec');
                        }
                    }else if(ox >= o2 && ox < o3){
                        //D
                        if(animateobj.hover != 4){
                            animateobj.hover = 4;
                            animateobj.overd.current = 1;
                            $(animateobj.sel + " .mov").removeClass('overa');
                            $(animateobj.sel + " .mov").removeClass('overb');
                            $(animateobj.sel + " .mov").removeClass('overc');
                            $(animateobj.sel + " .mov").removeClass('overd');
                            $(animateobj.sel + " .mov").removeClass('overe');
                            $(animateobj.sel + " .mov").addClass('overd');
                            $("#closet-box02").addClass('actived');
                        }
                    }else if(ox >= o3 && ox < o4){
                        //E
                        if(animateobj.hover != 5){
                            animateobj.hover = 5;
                            animateobj.overe.current = 1;
                            $(animateobj.sel + " .mov").removeClass('overa');
                            $(animateobj.sel + " .mov").removeClass('overb');
                            $(animateobj.sel + " .mov").removeClass('overc');
                            $(animateobj.sel + " .mov").removeClass('overd');
                            $(animateobj.sel + " .mov").removeClass('overe');
                            $(animateobj.sel + " .mov").addClass('overe');
                            $("#closet-box02").addClass('activee');
                        }
                    }
                });


                setInterval(function(){
                    var animateobj =  AEONFANTASYCLOSET.animates[0];
                    if(animateobj.hover == 0){
                        animateobj.idle.current ++;
                        if(animateobj.idle.current > animateobj.idle.loop){
                            animateobj.idle.current = animateobj.idle.start;
                        }
                    }else if(animateobj.hover == 1){
                        animateobj.overa.current ++;
                        if(animateobj.overa.current > animateobj.overa.loop){
                            animateobj.overa.current = animateobj.overa.start;
                        }
                    }else if(animateobj.hover == 2){
                        animateobj.overb.current ++;
                        if(animateobj.overb.current > animateobj.overb.loop){
                            animateobj.overb.current = animateobj.overb.start;
                        }
                    }else if(animateobj.hover == 3){
                        animateobj.overc.current ++;
                        if(animateobj.overc.current > animateobj.overc.loop){
                            animateobj.overc.current = animateobj.overc.start;
                        }
                    }else if(animateobj.hover == 4){
                        animateobj.overd.current ++;
                        if(animateobj.overd.current > animateobj.overd.loop){
                            animateobj.overd.current = animateobj.overd.start;
                        }
                    }else if(animateobj.hover == 5){
                        animateobj.overe.current ++;
                        if(animateobj.overe.current > animateobj.overe.loop){
                            animateobj.overe.current = animateobj.overe.start;
                        }
                    }

                    var pxi = (animateobj.idle.frames * animateobj.width) - (animateobj.idle.current - 1) * animateobj.width;
                    var pxa = (animateobj.overa.frames * animateobj.width) - (animateobj.overa.current - 1) * animateobj.width;
                    var pxb = (animateobj.overb.frames * animateobj.width) - (animateobj.overb.current - 1) * animateobj.width;
                    var pxc = (animateobj.overc.frames * animateobj.width) - (animateobj.overc.current - 1) * animateobj.width;
                    var pxd = (animateobj.overd.frames * animateobj.width) - (animateobj.overd.current - 1) * animateobj.width;
                    var pxe = (animateobj.overe.frames * animateobj.width) - (animateobj.overe.current - 1) * animateobj.width;

                    $(animateobj.sel + " .mov .idle").css("background-position", pxi + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overa").css("background-position", pxa + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overb").css("background-position", pxb + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overc").css("background-position", pxc + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overd").css("background-position", pxd + "px " + 0 +  "px");
                    $(animateobj.sel + " .mov .overe").css("background-position", pxe + "px " + 0 +  "px");

                }, 1000 / 24);
            }
        });
    }
};



AEONFANTASYCOMMON = {
    preload: function (images, progress) {
        var total = images.length;
        $(images).each(function(){
            var src = this;
            $('<img/>').attr('src', src).load(function() {
                images.remove(src);
                progress(total, total - images.length);
            });
        });
    }
};

Array.prototype.remove = function(element) {
  for (var i = 0; i < this.length; i++){
    if (this[i] == element) {
        this.splice(i,1);
    }
  }
};

// Google Analytics
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18444092-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
