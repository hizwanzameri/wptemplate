
	<!-- div#footer -->
<div id="wrap-footer">
<div id="footer">

<table cellpadding="0" cellspacing="0" class="table_footer_menu">
<tr>
<td class="aeon"><img src="<?php echo get_template_directory_uri(); ?>/images/aeon.gif" alt="aeon" width="120" height="36" /></td>
<td class="home"><a href="http://www.aeon.info/" target="_blank"></a></td>
<td class="link">
<ul class="footer_aeon_menu">	
<li><a href="/labs/aeo" target="_blank">Home</a></li>
<li><a href="<?php echo get_page_link(13); ?>" target="_blank">About Us</a></li>
<li><a href="<?php echo get_page_link(236); ?>" target="_blank">House Brand</a></li>
<li><a href="<?php echo get_page_link(189); ?>" target="_blank">Playground</a></li>
<li class="footer_aeon_menu_last"><a href="<?php echo get_page_link(76); ?>" target="_blank">Contact Us</a></li>
</ul>
<ul class="footer_aeon_menu">
<li><a href="<?php echo get_page_link(114); ?>" target="_blank">Birthday Packages</a></li>
<li class="footer_aeon_menu_last"><a href="<?php echo get_page_link(42); ?>" target="_blank">Characters</a></li>
</ul>
</td>
</tr>
</table>


<p id="footer_copyright">Copyright ©2015 Aeon Fantasy Group Malaysia. All rights reserved.</p>

</div>
</div>
<!-- end of div#footer -->
<?php wp_footer(); ?>
</body>
</html>