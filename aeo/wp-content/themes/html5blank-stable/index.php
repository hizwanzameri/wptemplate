<?php get_header(); ?>

<body id="home">
	<section id="container-home">
		<header id="header">
			<h1 id="siteid"><img src="<?php echo get_template_directory_uri(); ?>/images/siteid.png" width="244" height="160" alt="Aeon Fantasy"></h1>
		<!-- グロナビ 
			<nav id="gnav">
				<ul>
					<li class="hnav-home"><a href="<?php get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-home_off.png" width="88" height="50" alt="ホーム"></a></li><li class="hnav-event"><a href="<?php echo get_page_link(8); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-new_off.png" width="100" height="50" alt=""></a></li>
					<li><a href="<?php echo get_page_link(78); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-play_off.png" width="100" height="50" alt=""></a></li><li><a href="<?php echo get_page_link(71); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-location_off.png" width="88" height="50" alt=""></a></li><li><a href="<?php echo get_page_link(76); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-contact_off.png" alt="English" width="88" height="50"></a></li><li><a href="<?php echo get_page_link(13); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-about_off.png" width="88" height="50" alt=""></a></li>
				</ul>
			</nav>
			-->
			<nav id="primary_nav_wrap">
<ul>
  <li class="current-menu-item"><a href="<?php get_home_url(); ?>">Home</a></li>
  <li class="current-menu-item"><a href="#">About Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(13); ?>">Corporate Info</a></li>
      <li><a href="<?php echo get_page_link(36); ?>">Top Message</a></li>
      <li><a href="<?php echo get_page_link(80); ?>">Work With Us</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="#">What's New</a>
    <ul>
      <li><a href="<?php echo get_page_link(85); ?>">Events</a></li>
      <li><a href="<?php echo get_page_link(8); ?>">New Promotion</a></li>
      <li><a href="<?php echo get_page_link(114); ?>">Birthday Packages</a></li>
      <li><a href="<?php echo get_page_link(331); ?>">Group Visit Packages</a></li>
      <li><a href="<?php echo get_page_link(339); ?>">New Store Opening</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="<?php echo get_page_link(236); ?>">House Brand</a></li>
  <li class="current-menu-item"><a href="#">Playground</a>
    <ul>
      <li><a href="<?php echo get_page_link(42); ?>">Character</a></li>
      <li><a href="<?php echo get_page_link(189); ?>">Games</a></li>
      <li><a href="<?php echo get_page_link(193); ?>">Song</a></li>
      <li><a href="<?php echo get_page_link(195); ?>">Video</a></li>
      <li><a href="<?php echo get_page_link(249); ?>">Wallpaper</a></li>
    </ul>
    </li>
  <li class="current-menu-item"><a href="#">Contact Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(71); ?>">Headquarter</a></li>
      <li><a href="<?php echo get_page_link(164); ?>">Stores</a></li>
    </ul>
    </li>
</ul>
</nav>
</body>
		</header>
		<div id="tree">
			<div id="home-content-area">
				<img src="<?php echo get_template_directory_uri(); ?>/images/wood-main.png" width="960" height="1508" alt="" id="maintree">

		<!-- ヘッダー4つナビゲーション -->
				<nav id="hnav">
					<ul>
						<li class="hnav-company"><a href="<?php echo get_page_link(8); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/hnav-whatsnew_off.png" width="170" height="62" alt=""></a></li><!--
					--><li class="hnav-shop"><a href="<?php echo get_page_link(236); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/hnav-housebrand_off.png" width="170" height="62" alt=""></a></li><!--
					--><li class="hnav-english" id="merchnav"><a href="<?php echo get_page_link(42); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/hnav-playground_off.png" width="170" height="62" alt=""></a>
					<!--<ul>
      <li><a href="<?php echo get_page_link(42); ?>">Character</a></li>
      <li><a href="<?php echo get_page_link(189); ?>">Games</a></li>
      <li><a href="<?php echo get_page_link(193); ?>">Song</a></li>
      <li><a href="<?php echo get_page_link(195); ?>">Video</a></li>
      <li><a href="<?php echo get_page_link(249); ?>">Wallpaper</a></li>
    </ul>
					--></li>
					<li class="hnav-asia" id="merchnav"><a href="<?php echo get_page_link(62); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/hnav-merchandise_off.png" width="170" height="62" alt=""></a>
					<!-- <ul>
      <li><a href="<?php echo get_page_link(62); ?>">Redemption</a></li>
      <li><a href="<?php echo get_page_link(164); ?>">Selling Products</a></li>
    </ul>
					</li><!--
					-->
					</ul>
				</nav>

		<!-- Home contents -->
				<div id="box-youtube" class="home-content"><a href="<?php echo get_page_link(195); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/images/movie-thumb.png" alt="" class="movie"></a>
				</div>

				<div id="box-library" class="home-content">
					<a href="<?php echo get_page_link(261); ?>" class="button-action aeo">
						<div id="anime-AEO" class="home-anime">
							<div class="mov">
								<div class="idle"></div>
								<div class="over"></div>
							</div>
						</div>
					</a>
				</div>
				<div id="box-new" class="home-content">
					<a href="<?php echo get_page_link(85); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/box-new_off.png" width="136" height="136" alt=""></a>
				</div>

				<div id="box-lala" class="home-content">
					<a href="<?php echo get_page_link(400); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/box-lala_off.png" width="255" height="203" alt=""></a>
				</div>

				<div id="box-restaurant" class="home-content">
					<img src="<?php echo get_template_directory_uri(); ?>/images/box-restaurant.png" width="490" height="520" alt="">
				</div>

				<div id="box-workshop" class="home-content">
					<a href="<?php echo get_page_link(259); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/box-workshop_off.png" width="281" height="226" alt=""></a>
				</div>

				<div id="box-event" class="home-content">
					<a href="<?php echo get_page_link(8); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/box-promo_off.png" width="447" height="261" alt=""></a>
				</div>

				<div id="box-shop" class="home-content">
					<a href="<?php echo get_page_link(236); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/box-housebrand_off.png" width="321" height="282" alt=""></a>
				</div>

				<div id="box-company" class="home-content">
					<a href="<?php echo get_page_link(13); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/box-aboutus_off.png" width="209" height="155" alt=""></a>
				</div>

			<!-- animation character -->
				<div id="anime-LALA" class="home-anime button-action lala">
					<a href="#">
					<div class="mov">
						<div class="idle"></div>
						<div class="over"></div>
					</div>
					</a>
				</div>

				<div id="anime-PYUTAN" class="home-anime button-action pyutan">
					<a href="#">
					<div class="mov">
						<div class="idle"></div>
						<div class="over"></div>
					</div>
					</a>
				</div>

				<div id="anime-PANBIT" class="home-anime button-action panbit">
					
					<div class="mov">
						<div class="idle"></div>
						<div class="over"></div>
					</div>
					
				</div>
				<div id="anime-Smoke" class="home-anime">
					<img src="<?php echo get_template_directory_uri(); ?>/images/anime-smoke.gif" width="107" height="85" alt="">
				</div>

				<!-- SNS button -->
				<ul id="sns-box">
					<li class="sns-fb"><a href="https://www.facebook.com/mollyfantasymalaysia.my" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_1_fb.png" width="55" height="55" alt=""></a></li>
					<li class="sns-kids"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_5_shop.png" width="55" height="55" alt=""></a></li>
					<!--<li class="sns-app"><a href="https://play.google.com/store/apps/details?id=com.aeonfantasy.lala&hl=ja" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_3_app.png" width="55" height="55" alt=""></a></li>-->
					<!--<li class="sns-line"><a href="http://www.aeonfantasy-shop.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_2_kids.png" width="55" height="55" alt=""></a></li>-->
					<!--<li class="sns-cart"><a href="https://store.line.me/stickershop/product/1075358/ja" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_4_line.png" width="55" height="55" alt=""></a></li>-->
					<!--<li class="sns-kodomo"><a href="http://www.kodomoku.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_6_kodomo.png" width="55" height="55" alt=""></a></li>-->
				</ul>

			</div>
		</div>
	</section>
	<!-- banner -->
	<div class="loading"><img src="<?php echo get_template_directory_uri(); ?>/images/movies/main/load.gif"></div>
	<div id="bn-area">
	</div>

<?php get_footer(); ?>