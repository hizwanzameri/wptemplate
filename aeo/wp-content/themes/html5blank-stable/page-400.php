<?php /* Template Name: Wardrobe Template */ ?>
<html lang="ja">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=960">
  <meta name="format-detection" content="telephone=no">
	<meta name="keywords" content="イオンファンタジー,Mollyfantasy,モーリーファンタジー,室内遊園地,インプレ,インドア・プレーグラウンド,ララちゃん,イオくん,カードゲーム,UFOキャッチャー,イオン,イオングループ" />
<meta name="description" content="ファミリー向け室内遊園地を全国のショッピングセンター内で展開。Mollyfantasy（モーリーファンタジー）遊技機器の紹介等。" />
  <title>Lala's Wardrobe</title>
  <!-- OGP -->
	<meta property="og:locale" content="UTF-8" />
	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />
	<meta property="twitter:image" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<!-- ICON -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/image/favicon.ico">

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">

  <!-- JS -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/aeonfantasy.js"></script>
	<!--[if lt IE 9]>
	<script src="../js/html5shiv.js"></script>
	<![endif]-->

</head>
<body id="closet">
	<section id="container-closet">
		<header id="header">
			<h1 id="siteid"><a href="/labs/aeo"><img src="<?php echo get_template_directory_uri(); ?>/images/siteid.png" width="244" height="160" alt="Aeon Fantasy"></a></h1>
		<!-- gloval navigation -->
<nav id="primary_nav_wrap">
<ul>
  <li class="current-menu-item"><a href="<?php get_home_url(); ?>">Home</a></li>
  <li class="current-menu-item"><a href="#">About Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(13); ?>">Corporate Info</a></li>
      <li><a href="<?php echo get_page_link(36); ?>">Top Message</a></li>
      <li><a href="<?php echo get_page_link(80); ?>">Work With Us</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="#">What's New</a>
    <ul>
      <li><a href="<?php echo get_page_link(85); ?>">Events</a></li>
      <li><a href="<?php echo get_page_link(8); ?>">New Promotion</a></li>
      <li><a href="<?php echo get_page_link(114); ?>">Birthday Packages</a></li>
      <li><a href="<?php echo get_page_link(331); ?>">Group Visit Packages</a></li>
      <li><a href="<?php echo get_page_link(339); ?>">New Store Opening</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="<?php echo get_page_link(236); ?>">House Brand</a></li>
  <li class="current-menu-item"><a href="#">Playground</a>
    <ul>
      <li><a href="<?php echo get_page_link(42); ?>">Character</a></li>
      <li><a href="<?php echo get_page_link(189); ?>">Games</a></li>
      <li><a href="<?php echo get_page_link(193); ?>">Song</a></li>
      <li><a href="<?php echo get_page_link(195); ?>">Video</a></li>
      <li><a href="<?php echo get_page_link(249); ?>">Wallpaper</a></li>
    </ul>
    </li>
  <li class="current-menu-item"><a href="#">Contact Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(71); ?>">Headquarter</a></li>
      <li><a href="<?php echo get_page_link(164); ?>">Stores</a></li>
    </ul>
    </li>
</ul>
</nav>
		</header>
		<div id="closet-content-area">
			<div id="closet-house">
				<img src="<?php echo get_template_directory_uri(); ?>/images/closet/bg-container.jpg" width="960" height="1000" alt="">
			</div>
			<div id="closet-box01">
				<img src="<?php echo get_template_directory_uri(); ?>/images/closet/closet01.png" width="261" height="220" alt="">
			</div>
			<div id="closet-box02"></div>
			<div class="mov">
				<div class="idle"></div>
				<div class="overa"></div>
				<div class="overb"></div>
				<div class="overc"></div>
				<div class="overd"></div>
				<div class="overe"></div>
			</div>
		</div>

<!--
<main role="main">
		<!-- section --/>
		<section>

			<h1 class="h1page"><?php the_title(); ?></h1>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article --/>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article --/>

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article --/>
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article --/>

		<?php endif; ?>

		</section>
		<!-- /section --/>
	</main>
-->

	</section>
	<div class="loading"><img src="<?php echo get_template_directory_uri(); ?>/images/movies/main/load.gif"></div>


	


<?php get_footer(); ?>
