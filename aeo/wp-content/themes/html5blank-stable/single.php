<?php get_header(); ?>

<body id="event" class="page">
	<div id="container">
		<header id="header">
			<h1 id="siteid"><a href="/labs/aeo"><img src="<?php echo get_template_directory_uri(); ?>/images/siteidS.png" width="174" height="108" alt="Aeon Fantasy"></a></h1>
		<!-- gloval navigation 
			<nav id="gnav">
				<ul>
					<li class="hnav-home"><a href="<?php get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-home_off.png" width="88" height="50" alt="ホーム"></a></li><!--
					--<li class="hnav-event"><a href="<?php echo get_page_link(8); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-new_off.png" width="100" height="50" alt=""></a></li><!--
					--<li><a href="<?php echo get_page_link(78); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-play_off.png" width="100" height="50" alt=""></a></li><!--
					--<li><a href="<?php echo get_page_link(71); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-location_off.png" width="88" height="50" alt=""></a></li><!--
					--<li><a href="<?php echo get_page_link(76); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-contact_off.png" alt="English" width="88" height="50"></a></li><!--
					--<li><a href="<?php echo get_page_link(13); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/gnav-about_off.png" width="88" height="50" alt=""></a></li>
				</ul>
			</nav>-->
						<nav id="primary_nav_wrap">
<ul>
  <li class="current-menu-item"><a href="/labs/aeo">Home</a></li>
  <li class="current-menu-item"><a href="#">About Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(13); ?>">Corporate Info</a></li>
      <li><a href="<?php echo get_page_link(36); ?>">Top Message</a></li>
      <li><a href="<?php echo get_page_link(80); ?>">Work With Us</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="#">What's New</a>
    <ul>
      <li><a href="<?php echo get_page_link(85); ?>">Events</a></li>
      <li><a href="<?php echo get_page_link(8); ?>">New Promotion</a></li>
      <li><a href="<?php echo get_page_link(114); ?>">Birthday Packages</a></li>
      <li><a href="<?php echo get_page_link(331); ?>">Group Visit Packages</a></li>
      <li><a href="<?php echo get_page_link(339); ?>">New Store Opening</a></li>
    </ul>
  </li>
  <li class="current-menu-item"><a href="<?php echo get_page_link(236); ?>">House Brand</a></li>
  <li class="current-menu-item"><a href="#">Playground</a>
    <ul>
      <li><a href="<?php echo get_page_link(42); ?>">Character</a></li>
      <li><a href="<?php echo get_page_link(189); ?>">Games</a></li>
      <li><a href="<?php echo get_page_link(193); ?>">Song</a></li>
      <li><a href="<?php echo get_page_link(195); ?>">Video</a></li>
      <li><a href="<?php echo get_page_link(249); ?>">Wallpaper</a></li>
    </ul>
    </li>
  <li class="current-menu-item"><a href="#">Contact Us</a>
    <ul>
      <li><a href="<?php echo get_page_link(71); ?>">Headquarter</a></li>
      <li><a href="<?php echo get_page_link(164); ?>">Stores</a></li>
    </ul>
    </li>
</ul>
</nav>
		</header>
		<div id="headtree"></div>
		<div id="content-area" class="content-page">

	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
				</a>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<!-- post title -->
			<h1>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			</h1>
			<!-- /post title -->

			<!-- post details -->
			<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
			<span class="author"><?php _e( 'Published by', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>
			<span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span>
			<!-- /post details -->

			<?php the_content(); // Dynamic Content ?>

			<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

			<p><?php _e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>

			<p><?php _e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>

			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

			<?php comments_template(); ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

	</div>
	</div>


<?php get_footer(); ?>
