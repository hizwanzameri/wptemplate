=== Photo Book Gallery ===
Contributors: Rameez_Iqbal
Tags: gallery, photos, images, book, shortcode, simple, pages, posts, menu, media uploader, animation, flipbook, flip pages, gallery book, album, photo album
Donate link: http://webcodingplace.com/contact-us/
Requires at least: 3.5
Tested up to: 4.2.2
Stable tag: 4.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

UNLIMITED Page Flip Books for Images with Custom Controls

== Description ==
Simple but Amazing Gallery Books for Images where you can add multiple images and sort them as per your need, Builtin media uploader is also added.
<br>
<a href="http://webcodingplace.com/photo-book-gallery-demo/">SEE DEMO</a><br>
<a href="http://webcodingplace.com/photo-book-gallery/">HELP and USAGE</a><br>
<h3>Features</h3>
<ul>
	<li>Multiple Shortcodes Support</li>
	<li>Sort Images</li>
	<li>Custom Width</li>
	<li>Custom Height</li>
	<li>Touch Support</li>
	<li>Keypress Support</li>
	<li>Animated Gallery</li>
	<li>Custom Speed of Turning Pages</li>
	<li>Custom Starting Page</li>
	<li>RTL and LTR Reading Directions</li>
	<li>Custom Image Padding</li>
	<li>Show/Hide Page Numbers</li>
	<li>Closed Book View</li>
	<li>AutoPlay</li>
	<li>AutoPlay Delay</li>
	<li>Turn Page by Clicking Image</li>
	<li>Navigation Tabs</li>
	<li>Arrows</li>
</ul>

== Installation ==
1. Go to plugins in your dashboard and select 'add new'
2. Search for 'Photo Book Gallery' and install it
3. Go to Dashboard > Photo Book and upload images as pages of book
4. Use this shortcode [photo-book-1] in any page
5. Now visit that page

== Screenshots ==
1. Admin Settings
2. Book in Action
3. Flip Pages

== Changelog ==

= 4.2 =
* Bug Fixed: Shortcode renders at top of the page is fixed.

= 4.1 =
* Now Supported for PHP < 5.3

= 4.0 =
* Advanced Configuration for Book

= 3.0 =
* Multiple Books with Multiple Shortcodes