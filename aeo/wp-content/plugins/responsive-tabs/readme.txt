=== Responsive Tabs ===
Contributors: spwebguy
Tags: tab, tabs, accordion, shortcode, content, responsive, responsive tab
Requires at least: 3.6
Tested up to: 4.2.2
Stable tag: trunk
License: GPL2
License URI: http://www.gnu.org/licenses/gpl.html

A responsive and clean way to display your content. Create new tabs in no-time (custom type) and copy-paste the shortcode into any post/page.

== Description ==
This plugin adds a “Tabs” section to the admin panel which allows you to create tabbed content for your website the easy way. You can quickly add your content to your different tabs, choose a color, a breakpoint, re-order them at anytime and display them anywhere with a simple shortcode. What you see is what you get, and it’s totally free.

= No limitation for the Free version =
The free version of the Responsive Tabs plugin is **not limited** and does not contain any ad.    

The [PRO version](http://wpdarko.com/items/responsive-tabs-pro/) gives you access to new features such as new styling settings and **icons** (from font-awesome).

= Available fields =
* Title
* Content (text/images/anything)

= Available settings =
* Breakpoint (decide when the tabs should shrink into an accordion)
* Color 

= Usage =
Go to [the plugin's documentation](http://wpdarko.com/support/documentation/get-started-responsive-tabs/) if you need more information on how to use this plugin.

= Support =
Find help in [our forums](http://wpdarko.com/support/forum/plugins/responsive-tabs/) for this plugin (we’ll answer you fast, promise).

== Installation ==

= Installation =
1. In your WordPress admin panel, go to Plugins > New Plugin
2. Find our Responsive Tabs plugin by WP Darko and click Install now
3. Alternatively, download the plugin and upload the contents of responsive-tabs.zip to your plugins directory, which usually is /wp-content/plugins/
4. Activate the plugin

= Usage =
Go to [the plugin's documentation](http://wpdarko.com/support/documentation/get-started-responsive-tabs/) for information on how to use it.

== Frequently Asked Questions ==
= No limitation for the Free version =
The free version of the Responsive Tabs plugin is **not limited** and does not contain any ad.    

The [PRO version](http://wpdarko.com/items/responsive-tabs-pro/) gives you access to new features such as new styling settings and **icons** (from font-awesome).

= Usage =
Go to [the plugin's documentation](http://wpdarko.com/support/documentation/get-started-responsive-tabs/) if you need more information on how to use this plugin.

= Support =
Find help in [our forums](http://wpdarko.com/support/forum/plugins/responsive-tabs/) for this plugin (we’ll answer you fast, promise).

== Screenshots ==
1. Displaying the tabs (front view)
2. Displaying the tabs 2 (front view)
3. Creating tabs (admin view)
4. Finding the shortcode (admin view)

== Changelog ==
= 3.0.2 =
* SAFE UPDATE: from 2.0 or higher
* Fixes the update process (from 2.0 to 3.0)

= 3.0 =
* SAFE UPDATE: from 2.0 or higher
* New admin interface (new framework)
* Allow forcing plugin's original fonts
* Added docs and support forums links
* Added instructions
* Cleaning code

= 2.0 =
* WARNING: STYLING CHANGES (NO DATA LOSS) 
* Major styling bug fixes (spacing, fonts)
* Better adapts to your theme
* Bigger content field when editing your tabs (UX)
* Fixed 1 PHP errors
* Cleaning code

= 1.3.1 =
* SAFE UPDATE: from 1.0 or higher
* Added a unique class to tab’s links for customization (user request)

= 1.3 =
* SAFE UPDATE: from 1.0 or higher
* Added nested shortcode support
* Styling bug fixes for many themes

= 1.2.1 =
* SAFE UPDATE: from 1.0 or higher
* Minor bug fixes

= 1.2 =
* SAFE UPDATE: from 1.0 or higher
* Fixed background problem when add images to the tabs

= 1.1.2 =
* SAFE UPDATE: from 1.0 or higher
* Minor bug fixes
* Preparing for WordPress 4.1

= 1.1.1 =
* SAFE UPDATE: from  1.0 or higher
* Minor bug fixes
* Prevent conflicts between plugins 

= 1.1 =
* SAFE UPDATE: from 1.0
* Minor CSS bug fixes
* Preventing conflicts between plugins 
* Automatic recovery of all tabs when switching to PRO version

= 1.0 =
* Initial release (yay!)
